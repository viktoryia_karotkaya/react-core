import React from "react";
import './App.css';
import Header from "./Header";
import Counter from "./Counter";
import TotalCounter1 from "./TotalCounter1";
import TotalCounter2 from "./TotalCounter2";
import Memo from "./Memo";


const MyFooter = React.createElement(
  'footer',
  {style: {
      fontSize: '15px',
      textAlign: "right",
      background: '#84a2b7',
      color: "black",
      padding: '15px 10px'
    }},
  'Author: Viktoryia Karotkaya'
);

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      counter1: 0,
      counter2: 0,
      evenCounter1: true
    };
    //1 approach is to bind the context inside constructor, we create a function wrapper with binded this
    //it is better approach because others (2 an 3) of binding create new function every time when render, what affects performance
    this.increaseCounter1 = this.increaseCounter1.bind(this);
  }
  
  increaseCounter1() {
    let currentCounter = this.state.counter1 + 1;
    this.setState({
      counter1: currentCounter,
      evenCounter1: !this.state.evenCounter1
    });
  }
  
  decreaseCounter1() {
    let currentCounter = this.state.counter1 - 1;
    this.setState({
      counter1: currentCounter,
      evenCounter1: !this.state.evenCounter1
    });
  }
  
  increaseCounter2() {
    let currentCounter = this.state.counter2 + 1;
    this.setState({
      counter2: currentCounter
    });
  }
  
  decreaseCounter2 = () => {
    let currentCounter = this.state.counter2 - 1;
    this.setState({counter2: currentCounter});
  }
  
  render() {
    return (
      <>
        <Header />
        <TotalCounter1
          counter={this.state.counter1}
          even={this.state.evenCounter1}  />
        <Counter
          name={'Counter 1'}
          increase={this.increaseCounter1}
          //2 approach to use arrow function, arrow function has the same context as when it is defined
          decrease={() => this.decreaseCounter1()} />
        <TotalCounter2 counter={this.state.counter2} />
        <Counter
          name={'Counter 2'}
          //3 approach to create function wrapper with binded this inside render
          increase={this.increaseCounter2.bind(this)}
          decrease={this.decreaseCounter2} />
        <Memo counter2={this.state.counter2} />
        {MyFooter}
      </>
    )
  }
}

export default App;

