import React from "react";

const Counter = ({name, increase, decrease}) => {
  return (
    <main className="Counter">
      <div className="Counter-buttons-wrapper">
        <button onClick={increase}>Increase {name}</button>
        <button onClick={decrease}>Decrease {name}</button>
      </div>
    </main>
  )
}

export default Counter;
