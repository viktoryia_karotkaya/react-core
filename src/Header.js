import React from "react";
import logo from "./logo.svg";

class Header extends React.PureComponent {
  
  render() {
    return (
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>Core Concepts</h1>
      </header>
    )
  }
}

export default Header;
