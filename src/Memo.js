import React from "react";

const Memo = ({counter2}) => {
  console.log('Memo component calls');
  return (
      <div>
        <h3>I rerender when counter 2 changes</h3>
        <p>{counter2}</p>
      </div>
  )
}

export default React.memo(Memo);
