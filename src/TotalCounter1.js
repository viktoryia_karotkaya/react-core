import React from "react";

class TotalCounter1 extends React.PureComponent {
  render() {
    console.log('TotalCounter1 render is calling');
    return (
      <div>
        <h3>
          Counter 1 equals {this.props.counter}
        </h3>
        <p style={this.props.even ? {color: 'green'} : {color: 'yellow'}}>
          {this.props.even ? 'Counter 1 is even' : 'Counter 1 is odd'}
        </p>
      </div>
    )
  }
}

export default TotalCounter1;
