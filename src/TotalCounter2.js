import React from "react";

class TotalCounter2 extends React.Component {
  shouldComponentUpdate(nextProps, nextState, nextContext) {
    console.log('TotalCounter2 shouldComponentUpdate is calling')
    return nextProps.counter !== this.props.counter;
  }
  
  render() {
    console.log('TotalCounter2 render is calling');
    return (
      <div>
        <h3>
          Counter 2 equals {this.props.counter}
        </h3>
      </div>
    )
  }
}

export default TotalCounter2;
